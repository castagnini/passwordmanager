# the makefile :)

all: build_client run_server

GOENV != go env GOROOT

GOLDFLAGS = -ldflags "-w -s"

build_client:
	GOARCH=wasm GOOS=js go build ${GOLDFLAGS} -o ./target/client.wasm  ./client/client.go
	cp ${GOENV}/misc/wasm/wasm_exec.js ./target/

build_server:
	go build -o ./target/server ./server/server.go

run_server:
	go run server/server.go

clean:
	rm -f target/client.wasm
	rm -f target/server
	rm -f target/wasm_exec.js

