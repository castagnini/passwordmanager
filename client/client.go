package main

import (
  "fmt"
  "syscall/js"
  "net/http"
  "net/url"
  "io/ioutil"
  "crypto/rsa"
  "crypto/rand"
  "crypto/sha512"
  "math/big"
)

var serverKey rsa.PublicKey

func sendAsync(user string, pass string) {
  formData := url.Values{
   "user": {user},
   "password": {pass},
  }
  resp, err := http.PostForm("/form", formData)
  if err != nil {
    panic(err)
  }
  defer resp.Body.Close()

  doc := js.Global().Get("document")
  body, err := ioutil.ReadAll(resp.Body)
  doc.Call("getElementById", "myform").Set("innerHTML", string(body))
}

func send(this js.Value, inputs []js.Value) interface{} {
  doc := js.Global().Get("document")
  username := doc.Call("getElementById", "user").Get("value").String()
  password := doc.Call("getElementById", "password").Get("value").String()
  go sendAsync(encryptWithPublicKey(username),
               encryptWithPublicKey(password))
  return nil
}

// encryptWithPublicKey encrypts data with public key
func encryptWithPublicKey(msg string) string {
  msg_byte := []byte(msg)
	hash := sha512.New()
	ciphertext, err := rsa.EncryptOAEP(hash, rand.Reader, &serverKey, msg_byte, nil)
	if err != nil {
		panic(err)
	}
	return string(ciphertext)
}


func getKey() {

  resp, err := http.Get("/key")
  if err != nil {
    panic(err)
  }
  defer resp.Body.Close()

  body, err := ioutil.ReadAll(resp.Body)
  if err != nil {
    panic(err)
  }
  var exponent int
  var mod  string
  fmt.Sscanf(string(body),"{%s %d}", &mod, &exponent)
  bigN := new(big.Int)
  _, ok := bigN.SetString(mod, 10)
  if !ok {
    panic("failed to parse")
  }
  serverKey = rsa.PublicKey{
    N: bigN,
    E: exponent,
  }
  println(exponent)
}

func main() {
  ca := make(chan bool)
  go getKey()
  doc := js.Global().Get("document")
  doc.Call("getElementById", "myform").Set("innerHTML", "wasm loaded!")
  js.Global().Set("send",js.FuncOf(send))
  <-ca
}




