// +build ignore

package main

import (
  "fmt"
  "log"
  "net/http"
  "crypto/rsa"
  "crypto/rand"
  "crypto/sha512"
)

var globalKey *rsa.PrivateKey

func formHandler(w http.ResponseWriter, r *http.Request) {
    if err := r.ParseForm(); err != nil {
        fmt.Fprintf(w, "ParseForm() err: %v", err)
        return
    }
    fmt.Fprintf(w, "POST request successful\n")
    user := r.FormValue("user")
    password:= r.FormValue("password")


    fmt.Fprintf(w, "User = %s\n", decryptWithPrivateKey(user))
    fmt.Fprintf(w, "Password = %s\n", decryptWithPrivateKey(password))
}

// decryptWithPrivateKey decrypts data with private key
func decryptWithPrivateKey(ciphertext string) string {
  ciphertext_byte := []byte(ciphertext)
	hash := sha512.New()
	plaintext, err := rsa.DecryptOAEP(hash, rand.Reader, globalKey, ciphertext_byte, nil)
	if err != nil {
		panic(err)
	}
	return string(plaintext)
}

func keyHandler(w http.ResponseWriter, r *http.Request) {
    fmt.Fprintf(w, "%v", globalKey.PublicKey)
}

func main() {
    var err error
    globalKey, err = rsa.GenerateKey(rand.Reader,  4096)
	fmt.Println("key generate: E=%v", globalKey.E)
    if err != nil {
      panic("Cannot generate RSA key")
    }
    //fmt.Printf("%#v\n", globalKey.PublicKey)  

    fileServer := http.FileServer(http.Dir("./target"))
    http.Handle("/", fileServer)
    http.HandleFunc("/form", formHandler)
    http.HandleFunc("/key", keyHandler)
    log.Fatal(http.ListenAndServe(":8080", nil))
}
